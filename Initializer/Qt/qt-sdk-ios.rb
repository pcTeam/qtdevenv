require "Initializer/Qt/qt-sdk-base.rb"

module QDE
module Initializer
module Qt

class QtSDKiOS < QtSDKBase
	def configure_internal(config)
		config.xplatform("macx-ios-clang")
		config.ios(@options)
		# config.nomake("tests")
	end
end

end
end
end