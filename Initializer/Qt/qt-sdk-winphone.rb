require "Initializer/Qt/qt-sdk-base.rb"

module QDE
module Initializer
module Qt

class QtSDKWinPhone < QtSDKBase
	def configure_internal(config)
		config.winphone(@options)
		config.noopenssl()
	end
end

end
end
end