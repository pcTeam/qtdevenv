module QDE
module Initializer
module Qt

class InitRepository
	def initialize(path = nil)
		@path = !path.nil? ? path : "."
		@arguments = {}
		@submodules = []
	end

	def << (submodule)
		@submodules << submodule
	end

	def skipWebKit
		@arguments["--no-webkit"] = nil
	end

	def run
		arguments = @arguments.map{|k,v| v.nil? ? k : "#{k}=#{v}"}.join(' ')
		if not @submodules.empty?
			arguments << " --module-subset=#{@submodules.join(',')}"
		end

		interpreter = Gem.win_platform? ? "perl " : ""

		command = "#{interpreter} #{@path}/init-repository #{arguments} --mirror https://gitorious.org/"
		puts "Initializing repository: #{Dir.pwd} - #{command}"
		puts "Environment: #{ENV['GIT_WORK_TREE']}"
		system(command)
	end

end

end
end
end