require "Initializer/Qt/qt-sdk-base.rb"

module QDE
module Initializer
module Qt

class QtSDKBlackBerry < QtSDKBase
	def prerequisities
		ENV["QNX_TARGET"] = "#{@options[:sdk]}/target_10_2_0_1155/qnx6"
		ENV["QNX_HOST"] = "#{@options[:sdk]}/host_10_2_0_15/linux/x86"
		ENV["QNX_CONFIGURATION"] = "/home/korvin/.rim/bbndk"
		ENV["MAKEFLAGS"] = "-I#{@options[:sdk]}/target_10_2_0_1155/qnx6/usr/include"
		ENV["LD_LIBRARY_PATH"] = "#{@options[:sdk]}/host_10_2_0_15/linux/x86/usr/lib:#{@options[:sdk]}/target_10_2_0_1155/qnx6/../linux/x86/usr/lib/qt5/lib:#{ENV['LD_LIBRARY_PATH']}"
		ENV["PATH"] = "#{@options[:sdk]}/host_10_2_0_15/linux/x86/usr/bin/qt5:#{@options[:sdk]}/host_10_2_0_15/linux/x86/usr/bin:/home/korvin/.rim/bbndk/bin:#{@options[:sdk]}/host_10_2_0_15/linux/x86/usr/python32/bin:#{ENV['PATH']}"
		ENV["QT_PLUGIN_PATH"] = "#{@options[:sdk]}/target_10_2_0_1155/qnx6/../linux/x86/usr/lib/qt5/plugins"
		ENV["QT_LIB_PATH"] = "#{@options[:sdk]}/target_10_2_0_1155/qnx6/../linux/x86/usr/lib/qt5/lib"
		ENV["QML2_IMPORT_PATH"] = "#{@options[:sdk]}/target_10_2_0_1155/qnx6/../linux/x86/usr/lib/qt5/qml"
	end

	def configure_internal(config)
		config.xplatform("blackberry-armle-v7-qcc")
		config.openssl()
		config.opengl("es2")
		config.blackberry(:slog2 => false)
	end
end

end
end
end