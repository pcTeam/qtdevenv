require "Initializer/Qt/qt-sdk-base.rb"

module QDE
module Initializer
module Qt

class QtSDKAndroid < QtSDKBase
	def configure_internal(config)
		config.xplatform("android-g++")
		config.openssl()
		config.android(@options)
	end
end

end
end
end