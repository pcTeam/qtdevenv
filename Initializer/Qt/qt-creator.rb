require "Tools/Common/qmake.rb"
require "Tools/Common/make.rb"

module QDE
module Initializer
module Qt

class QtCreator
	def initialize(repository = nil, version = nil)
		@repository = !repository.nil? ? repository : "https://gitorious.org/qt-creator/pteam-qt-creator.git"
		@version = !version.nil? ? version : "latest"
	end

	def prepare(qmake)
		@base = Dir.pwd
		@qmake = qmake
		FileUtils::rm_rf("Temp")
		FileUtils::mkdir_p 'Temp'
		Dir.chdir("Temp") do
			clone()
			Dir.chdir("QtCreator") do
				qmake()
				build()
				install()
			end
		end		
	end

	def clone
		puts "Clonning"
		g = Git.clone(@repository, "QtCreator", :recursive => true)
		g.checkout(@version)
		ENV.delete('GIT_WORK_TREE')
	end

	def qmake
		qmake = Tools::Common::QMake.new(@qmake)
		qmake.recursive
		qmake.run("qtcreator.pro")
	end
	
	def build
		make = Tools::Common::Make.new
		make.build
	end

	def install
		make = Tools::Common::Make.new
		make.parameter(:INSTALL_ROOT => "#{@base}/Environment/QtCreator/#{@version}")
		make.install
	end
end

end
end
end