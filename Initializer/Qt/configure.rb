module QDE
module Initializer
module Qt

class Configure
	def initialize(path = nil)
		@path = !path.nil? ? path : "."
		@arguments = {}
		@nomake = []
	end

	def static
		@arguments["-static"] = nil
	end

	def open_source
		@arguments["-opensource"] = nil
	end

	def comercial
		@arguments["-commercial"] = nil
	end

	def confirm_license
		@arguments["-confirm-license"] = nil
	end

	def debug
		@arguments.delete("-release")
		@arguments["-debug"] = nil
	end

	def release
		@arguments.delete("-debug")
		@arguments.delete("-developer-build")
		@arguments["-release"] = nil
	end

	def developer_build
		@arguments.delete("-release")
		@arguments.delete("-prefix")
		@arguments["-developer-build"] = nil
	end

	def prefix(path)
		@arguments["-prefix"] = path
	end

	def nomake(part)
		@nomake << part
	end

	def xplatform(name)
		@arguments["-xplatform"] = name
	end

	def openssl()
		@arguments["-openssl"] = nil
		@arguments["-I"] = "/home/korvin/Links/Programs/Android/openssl-1.0.1j/include"
	end

	def noopenssl()
		@arguments["-no-openssl"] = nil
	end

	def opengl(version)
		@arguments["-opengl"] = version
	end

	def apple(args)
		if args.key?(:sdk)
			@arguments["-sdk"] = args[:sdk]
		end
	end

	def blackberry(args) 
		if args.key?(:slog2)
			if args[:slog2]
				@arguments["-slog2"] = nil
			else
				@arguments["-no-slog2"] = nil
			end
		end
	end

	def android(args)
		if args.key?(:sdk)
			@arguments["-android-sdk"] = args[:sdk]
		end
		if args.key?(:ndk)
			@arguments["-android-ndk"] = args[:ndk]
		end
		if args.key?(:platform)
			@arguments["-android-ndk-platform"] = args[:platform]
		end
		if args.key?(:host)
			@arguments["-android-ndk-host"] = args[:host]
		end
		if args.key?(:arch)
			@arguments["-android-arch"] = args[:arch]
		end
		if args.key?(:toolchain)
			@arguments["-android-toolchain-version"] = args[:toolchain]
		end
	end

	def ios(args)
		if args.key?(:sdk)
			@arguments["-sdk"] = args[:sdk]
		end
	end

	def winphone(args)
		if args.key?(:mkspec)
			xplatform(args[:mkspec])
		end
	end

	def run
		arguments = @arguments.map{|k,v| v.nil? ? k : "#{k} #{v}"}.join(' ')
		if not @nomake.empty?
			arguments << ' ' << @nomake.map{ | n | "-nomake #{n}" }.join(' ')
		end
		
		command = "#{@path}/configure #{arguments}"
		puts "Configuring repository: #{command}"
		system(command)
	end
end

end
end
end