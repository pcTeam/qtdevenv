require 'fileutils'
require 'git'
require "Initializer/Qt/init-repository.rb"
require "Initializer/Qt/configure.rb"
require "Tools/Common/make.rb"
require "Tools/Debian/apt.rb"

module QDE
module Initializer
module Qt

class QtSDKBase
	def initialize(path, release = true, platform = nil, options = {}, submodules = [])
		@path = path
		@release = release
		@platform = !platform.nil? ? platform : "linux-g++"
		@options = options
		@submodules = submodules != [] ? submodules : [ "qtandroidextras", "qtbase", "qtdeclarative", "qtenginio", "qtgraphicaleffects", "qtimageformats", "qtlocation", "qtmultimedia", "qtquick1", "qtquickcontrols", "qtscript", "qtsvg", "qttools", "qttranslations", "qtx11extras", "qtxmlpatterns" ]
	end

	def prepare
		prerequisities()

		@base = Dir.pwd
		Dir.chdir(@path) do

			if (@release)
				source_dir = "Source"
			else
				source_dir = "#{@platform}/Debug"
				FileUtils::mkdir_p "#{@platform}"
				FileUtils.cp_r("Source", "#{@platform}")
				File.rename "#{@platform}/Source", "#{@platform}/Debug"
			end

			Dir.chdir(source_dir) do
				init()
				configure()
				build()
				install()
				clean()
			end
		end
	end

	def prerequisities
	end

	def init
		initRepository = InitRepository.new
		@submodules.each do | m |
			initRepository << m
		end		
		initRepository.run
	end

	def configure
		config = Configure.new
		config.open_source
		config.confirm_license
		if (@release)
			config.release
			config.prefix("#{@base}/#{@path}/#{@platform}/Release")
		else
			config.developer_build
		end
		config.nomake("examples")		
		configure_internal(config)
		config.run
	end

	def build
		make = Tools::Common::Make.new
		make.build
	end

	def install
		if (@release)
			make = Tools::Common::Make.new
			make.install
		end
	end

	def clean
		system("git submodule foreach --recursive 'git clean -dfx'")
	end
end

end
end
end