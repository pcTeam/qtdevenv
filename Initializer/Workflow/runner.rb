require 'Tools/Common/repository.rb'
require 'Initializer/Qt/qt-sdk.rb'
require 'Initializer/Qt/qt-sdk-ios.rb'
require 'Initializer/Qt/qt-sdk-android.rb'
require 'Initializer/Qt/qt-sdk-winphone.rb'
require 'Initializer/Qt/qt-sdk-blackberry.rb'
require 'Initializer/Qt/qt-creator.rb'
require 'Initializer/Workflow/continuous-integration.rb'

module QDE
module Initializer
module Workflow

class Runner
	def initialize(options)
	end
	
	def run
		puts "Initializer"
		FileUtils::rm_rf("Environment")

		repository = Tools::Common::Repository.new("git://code.qt.io/qt/qt5.git", "Qt", "v5.4.1")
		repository.clone("Environment")

		if RUBY_PLATFORM =~ /linux/
			sdk = Qt::QtSDK.new(repository.path, true)
			sdk.prepare

			sdk = Qt::QtSDK.new(repository.path, false)
			sdk.prepare


			sdk = Qt::QtSDKAndroid.new(repository.path, true, "android-g++", { :sdk => "/home/korvin/Links/Programs/Android/adt-bundle-linux-x86_64-20140321/sdk", :ndk => "/home/korvin/Links/Programs/Android/android-ndk-r9d", :host => "linux-x86_64" })
			sdk.prepare

			sdk = Qt::QtSDKAndroid.new(repository.path, false, "android-g++", { :sdk => "/home/korvin/Links/Programs/Android/adt-bundle-linux-x86_64-20140321/sdk", :ndk => "/home/korvin/Links/Programs/Android/android-ndk-r9d", :host => "linux-x86_64" })
			sdk.prepare


			sdk = Qt::QtSDKBlackBerry.new(repository.path, true, "blackberry-qcc", { :sdk => "/home/korvin/Programs/BlackBerry/bbndk" })
			sdk.prepare

			sdk = Qt::QtSDKBlackBerry.new(repository.path, false, "blackberry-qcc", { :sdk => "/home/korvin/Programs/BlackBerry/bbndk" })
			sdk.prepare
		elsif RUBY_PLATFORM =~ /darwin/
			v = /[\d\.]+/.match(`xcodebuild -showsdks | grep -o iphoneos.*$`)
			if v.nil?
				raise "iOS SDK not found"
			end
			version = v[0]

			# sdk = Qt::QtSDKiOS.new(nil, nil, true, "iphonesimulator-clang", { :sdk => "iphonesimulator#{version}" })
			# sdk.prepare

			sdk = Qt::QtSDKiOS.new(nil, nil, true, "iphone-clang", { :sdk => "iphoneos#{version}" })
			sdk.prepare
		elsif RUBY_PLATFORM =~ /win32/ or RUBY_PLATFORM =~ /x64-mingw32/
			sdk = Qt::QtSDKWinPhone.new(repository.path, true, "winphone-arm", { :mkspec => "winphone-arm-msvc2013" })
			sdk.prepare

			sdk = Qt::QtSDKWinPhone.new(repository.path, false, "winphone-arm", { :mkspec => "winphone-arm-msvc2013" })
			sdk.prepare

			sdk = Qt::QtSDKWinPhone.new(repository.path, true, "winphone-x86", { :mkspec => "winphone-x86-msvc2013" })
			sdk.prepare

			sdk = Qt::QtSDKWinPhone.new(repository.path, false, "winphone-x86", { :mkspec => "winphone-x86-msvc2013" })
			sdk.prepare
		else
			raise "Can't build Qt for '#{RUBY_PLATFORM}'"
		end
	end
end

end
end
end