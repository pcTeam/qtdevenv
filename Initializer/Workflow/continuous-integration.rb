require 'fileutils'
require "Tools/Common/vagrant.rb"
require "Initializer/Provisioning/build-server.rb"

module QDE
module Initializer
module Workflow

class ContinuousIntegration
	def run
		FileUtils::rm_rf("CI")
		FileUtils::mkdir_p 'CI'
		Dir.chdir("CI") do
			vagrant = Tools::Common::Vagrant.new("Master")
			vagrant.init("ubuntu/trusty64")
			vagrant << Provisioning::BuildServer.new({ :port => 8081, :type => :master })

			vagrant = Tools::Common::Vagrant.new("Linux")
			vagrant.init("ubuntu/trusty64")
			vagrant << Provisioning::BuildServer.new({ :port => 8082, :type => :slave })
			# vagrant << Provisioning::Linux.new
			# vagrant << Provisioning::Android.new
			# vagrant << Provisioning::BlackBerry.new

			# vagrant = Core::Tools::Vagrant.new("Windows")
			# vagrant.init(box_name)
			# vagrant << Provisioning::Windows.new
			# vagrant << Provisioning::WinPhone.new
			# vagrant << Provisioning::WinRT.new

			# vagrant = Core::Tools::Vagrant.new("Mac")
			# vagrant.init(box_name)
			# vagrant << Provisioning::Mac.new
			# vagrant << Provisioning::iOS.new
		end		
	end
end

end
end
end