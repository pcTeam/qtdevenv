module QDE
module Initializer
module Provisioning

class BuildServer
	def initialize(options = nil)
		@port = !options.nil? && options.include?(:port) ? options[:port] : 8080
		@type = !options.nil? && options.include?(:type) ? options[:type] : :master
	end

	def config
		[
			"config.vm.network :forwarded_port, host: #{@port}, guest: 8080",
			'config.vm.provision "shell", path: "bootstrap.sh", privileged: false'
		]
	end

	def steps
		[
			"sudo apt-get install ruby gem git-core -y", 
			"git clone https://pcteam:Merlyn123@bitbucket.org/pcteam/qtdevenv.git QtDevEnv", 
			"ruby QtDevEnv/qde.rb install -m build-server -t #{@type}",
		]
	end
end

end
end
end