require "optparse"
require "ostruct"

module QDE
module Options
module Subcommands

class Install 
	def name
		"install"
	end

	def description
		"installs the environment"
	end

	def parse
		options = OpenStruct.new 

		parser = OptionParser.new do | opts |
			opts.on("-m", "--module name", ["build-server"], "Module to install. Available modules: build-server") do | name |
        		options.module = name
        	end
        	opts.on("-t", "--type name", ["master", "slave"], "Build node type. Available modules: master, slave") do | name |
        		options.type = name
        	end
		end.parse!
		
		options
	end
end

end
end
end