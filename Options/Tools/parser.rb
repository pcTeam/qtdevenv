require "optparse"

module QDE
module Options
module Tools

class Parser
	def initialize
		@subcommands = []
	end

	def << (subcommand)
		@subcommands << subcommand
	end

	def parse(args)
		result = {}
		subtext = "Commonly used subcommands are:\n"
		@subcommands.each do | subcommand |
			subtext << "   " << subcommand.name + ":\t\t" + subcommand.description + "\n"
		end
		subtext << "\nSee 'qde.rb SUBCOMMAND --help' for more information on a specific subcommand."

		parser = OptionParser.new do | opts |
			opts.banner = "Usage: qde.rb subcommand [options]"
			opts.separator ""
			opts.separator subtext
		end

		result['subcommand'] = args.shift
		subcommand = @subcommands.find { | s | s.name == result['subcommand'] }
		if not subcommand.nil?
			result['options'] = subcommand.parse
		else
			puts "Invalid subcommand '#{result['subcommand']}'"
			puts parser.help
		end
		result
	end
end

end
end
end
