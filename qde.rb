#!/usr/bin/ruby

$LOAD_PATH.unshift("#{File.dirname(__FILE__)}")
require 'Options/Tools/parser.rb'
require 'Installer/Workflow/runner.rb'
require 'Options/Subcommands/install.rb'
require 'Options/Subcommands/init.rb'

module QDE
module System
module Console

class Runner
	def self.run

		parser = Options::Tools::Parser.new
		parser << Options::Subcommands::Install.new
		parser << Options::Subcommands::Init.new		
		result = parser.parse(ARGV)

		if not result['subcommand'].nil?
			if result['subcommand'] == 'install'
				installer = Installer::Workflow::Runner.new(result['options'])
				installer.run
			end
			if result['subcommand'] == 'init'
				require_relative 'Initializer/Workflow/runner.rb'
				initializer = Initializer::Workflow::Runner.new(result['options'])
				initializer.run
			end
		end
	end
end

Runner.run

end
end
end