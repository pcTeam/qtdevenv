require 'nokogiri'
require 'open-uri'

module QDE
module Tools
module Core

class WebPageParser
	def initialize(url)
		@url = url
		@templates_list = []
	end

	def << (templates)
		@templates_list << templates
	end

	def all_urls
		Nokogiri::HTML(open(@url).read).css("a").map do |link|
	    	link.attr("href")
	  	end.compact
	end

	def find_url
		urls = all_urls()
		@templates_list.each do | templates |
			urls.delete_if do | url | 
				found = false
				templates.each do | template |
					if (url.include? template or Regexp.new(template).match(url))
						found = true
					end
				end
				!found
			end
		end
		urls
	end
	
end

end
end
end