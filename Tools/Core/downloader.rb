require 'tempfile'
require 'open-uri'
require 'uri'

module QDE
module Tools
module Core

class Downloader
	def self.download(url)
		uri = URI.parse(url)
		FileUtils.rm_f(Dir.tmpdir + "/" + File.basename(uri.path))

		file = File.new(Dir.tmpdir + "/" + File.basename(uri.path), 'wb+')
		open(url, "rb", :read_timeout=>60) do |read_file|
			file.write(read_file.read)
		end
		file
	end
end

end
end
end