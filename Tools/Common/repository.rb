module QDE
module Tools
module Common

class Repository
	def initialize(repository, application, version)
		@repository = repository
		@application = application
		@version = version
	end

	def clone(basepath)
		@path = "#{basepath}/#{@application}/#{@version}"
		FileUtils::mkdir_p @path
		Dir.chdir(@path) do
			if not File.directory?("Source")
				puts "Clonning"
				g = Git.clone(@repository, "Source")
				ENV.delete('GIT_WORK_TREE')
				system("cd Source && git checkout origin/#{@version} -b #{@version}")
			end
		end
	end

	def path
		@path
	end

end

end
end
end