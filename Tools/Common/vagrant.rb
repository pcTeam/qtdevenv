module QDE
module Tools
module Common

class Vagrant
	def initialize(path)
		@path = !path.nil? ? path : "."
	end

	def init(box)
		FileUtils::rm_rf(@path)
		FileUtils::mkdir_p @path
		Dir.chdir(@path) do
			system("vagrant init #{box}")
		end
	end

	def << (provisioning)
		lines = []
		IO.foreach("#{@path}/Vagrantfile") do | line |
			lines << line
		end
		lines.insert(-2, provisioning.config.join("\n"))

		File.open("#{@path}/Vagrantfile", "w") do |f|
  			lines.each { |element| f.puts(element) }
		end

		open("#{@path}/bootstrap.sh", 'w') do |f|
		  	f.puts '#!/bin/bash'
		  	f.puts ""
		  	f.puts provisioning.steps.join("\n")
		end
	end

	def up
		Dir.chdir(@path) do
			system("vagrant up")
		end
	end
end

end
end
end