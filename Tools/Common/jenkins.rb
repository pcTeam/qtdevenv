module QDE
module Tools
module Common

class Jenkins
	def initialize(url = nil)
		@url = !url.nil? ? url : "http://127.0.0.1:8080"
	end

	def install_plugin(name)
		run("install-plugin #{name} -restart")
	end

	def run(options)
		command = "java -jar ~/.jenkins/jenkins-cli.jar -s #{@url} #{options}"
		puts "Executing jenkins command: #{command}"
		system(command)
	end
end

end
end
end