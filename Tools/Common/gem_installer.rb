module QDE
module Tools
module Common

class GemInstaller
	def initialize
		@gems = []
	end

	def << (name)
		@gems << name 
	end

	def install
		@gems.each do | name | 
			install_gem(name)
		end
		Gem.clear_paths
	end

	def install_gem(name)
		begin
		  gem name
		rescue LoadError
		  system("sudo gem install #{name}")		  
		end		
	end

	private :install_gem
end

end
end
end