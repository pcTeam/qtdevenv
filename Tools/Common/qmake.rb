module QDE
module Tools
module Common

class QMake
	def initialize(path = nil)
		@path = !path.nil? ? path : "qmake";
		@arguments = {}
	end

	def recursive
		@arguments["-r"] = nil
	end

	def parameter(hash)
		@arguments.merge(hash)
	end

	def run(file)
		arguments = @arguments.map{|k,v| v.nil? ? k : "#{k}=#{v}"}.join(' ')
		command = "#{@path} #{arguments} #{file}";
		puts "QMAKE: #{Dir.pwd} - #{command}"
		system(command)
	end

end

end
end
end