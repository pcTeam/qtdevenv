module QDE
module Tools
module Common

class Make
	def initialize
		@arguments = {}
	end

	def parameter(hash)
		@arguments.merge!(hash)
	end

	def build
		arguments = @arguments.map{|k,v| v.nil? ? k : "#{k}=#{v}"}.join(' ')
		cores = Gem.win_platform? ? "" : "-j#{number_of_processors() + 1}"
		system("#{Gem.win_platform? ? 'n' : ''}make #{cores} #{arguments}")
	end

	def install
		arguments = @arguments.map{|k,v| v.nil? ? k : "#{k}=#{v}"}.join(' ')
		command = "#{Gem.win_platform? ? 'n' : ''}make install #{arguments}"
		puts "INSTALLING: #{@arguments}"
		system(command)
	end

	def number_of_processors
  		if RUBY_PLATFORM =~ /linux/
    		return `cat /proc/cpuinfo | grep processor | wc -l`.to_i
  		elsif RUBY_PLATFORM =~ /darwin/
    		return `sysctl -n hw.logicalcpu`.to_i
  		elsif RUBY_PLATFORM =~ /win32/ or RUBY_PLATFORM =~ /x64-mingw32/
    		# this works for windows 2000 or greater
		    require 'win32ole'
		    wmi = WIN32OLE.connect("winmgmts://")
		    wmi.ExecQuery("select * from Win32_ComputerSystem").each do |system| 
		    begin
		    	processors = system.NumberOfLogicalProcessors
		    rescue
		    	processors = 0
		    end
		    return [system.NumberOfProcessors, processors].max
		end
	  	raise "can't determine 'number_of_processors' for '#{RUBY_PLATFORM}'"
	end
end

end

end
end
end