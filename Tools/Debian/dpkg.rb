module QDE
module Tools
module Debian

class Dpkg
	def initialize
		@packages = []
	end

	def << (package)
		@packages << package
	end

	def install
		system("sudo dpkg -i #{@packages.join(' ')}")
		system("sudo apt-get -f install -y")
		system("sudo apt-get autoremove -y")
		puts "Installing"
	end
end

end
end
end