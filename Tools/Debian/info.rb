module QDE
module Tools
module Debian

class Info
	def self.package_suffix
		return [".deb"];
	end

	def self.architecture
		return ['foo'].pack('p').size == 8 ? ["x64", "x86_64", "64", "AMD64"] : ["32", "x32", "x86", "i686", "i386"]
	end
end

end
end	
end