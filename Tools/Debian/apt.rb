module QDE
module Tools
module Debian

class Apt
	def initialize
		@packages = []
	end

	def << (package)
		@packages << package
	end

	def install
		puts "Installing packages: #{@packages.join(", ")}"
		system("sudo apt-get update")
		system("sudo apt-get install #{@packages.join(' ')} -y")
		system("sudo apt-get autoremove -y")
		puts "Installing"
	end

	def build_dep
		system("sudo apt-get update")
		system("sudo apt-get build-dep #{@packages.join(' ')} -y")
		system("sudo apt-get autoremove -y")
	end
end

end
end
end