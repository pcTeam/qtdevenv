require 'Tools/Common/gem_installer.rb'

module QDE
module Installer
module Master

class Dependencies
	def install
		g = QDE::Tools::Common::GemInstaller.new
		g << "colorize"
		g << "nokogiri"
		g << "git"
		g << "naturally"
		g.install()
	end
end

end
end
end