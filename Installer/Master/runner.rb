require 'Installer/Master/prerequisites.rb'
require 'Installer/Master/dependencies.rb'

module QDE
module Installer
module Master

class Runner
	def initialize(options)
	end

	def run
		prerequisites = Prerequisites.new
		prerequisites.install

		dependencies = Dependencies.new
		dependencies.install

		require_relative 'applications.rb'
		applications = Applications.new
		applications.install
	end
end

end
end
end