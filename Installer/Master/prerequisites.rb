require 'Tools/Debian/apt.rb'

module QDE
module Installer
module Master

class Prerequisites
	def install
		puts "Installing prerequisites..."
		apt = Tools::Debian::Apt.new
		apt << "build-essential" << "libxslt-dev" << "libxml2-dev" << "ruby-dev" << "git-core"
		apt.install
		puts "Installing prerequisites: Ok"
	end
end

end
end
end