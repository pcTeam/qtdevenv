require 'Installer/Applications/vagrant.rb'
require 'Installer/Applications/virtualbox.rb'

module QDE
module Installer
module Master

class Applications
	def install
		vagrant = Installer::Applications::Vagrant.new
		vagrant.install

		virtualbox = Installer::Applications::VirtualBox.new
		virtualbox.install

		# TODO: vmware
	end
end

end
end
end