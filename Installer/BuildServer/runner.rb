require "Installer/Master/prerequisites.rb"
require "Installer/Master/dependencies.rb"

module QDE
module Installer
module BuildServer

class Runner
	def initialize(options)
		@options = options
	end

	def run
		prerequisites = Master::Prerequisites.new
		prerequisites.install

		dependencies = Master::Dependencies.new
		dependencies.install

		require "Installer/Applications/jenkins.rb"
		jenkins = Applications::Jenkins.new
		jenkins.install

		if (@options.type.nil? or @options.type == "master")
			require "Tools/Common/jenkins.rb"
			cli = Tools::Common::Jenkins.new()
			cli.install_plugin(:swarm)
		elsif (@options.type == "slave")
			require "Installer/Applications/swarm.rb"
			swarm = Applications::Swarm.new
			swarm.install
		end
	end
end

end
end
end