require 'colorize'
require 'Tools/Core/web_page_parser.rb'
require 'Tools/Core/downloader.rb'
require 'Tools/Debian/info.rb'
require 'Tools/Debian/dpkg.rb'
require 'Tools/Debian/apt.rb'

module QDE
module Installer
module Applications 

class Jenkins
	def initialize(version = nil)
		@url = "http://pkg.jenkins-ci.org/debian/"
		@version = version
	end

	def install
		prerequisities()

		puts "Looking for jenkins: #{@url}".blue
		parser = Tools::Core::WebPageParser.new(@url)
		parser << Tools::Debian::Info.package_suffix
		if not @version.nil?
			parser << @version
		end
		parser << ["jenkins"]
		package_url = parser.find_url
		if (not package_url.empty?)
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download(@url + package_url.first)
			dpkg = Tools::Debian::Dpkg.new
			dpkg << package.path
			dpkg.install
			system("sudo /etc/init.d/jenkins status")
			sleep 20
			system("curl  -L http://updates.jenkins-ci.org/update-center.json | sed '1d;$d' | curl -X POST -H 'Accept: application/json' -d @- http://localhost:8080/updateCenter/byId/default/postBack")
			system("sudo /etc/init.d/jenkins restart")
			system("sudo /etc/init.d/jenkins status")
			sleep 20
			install_cli
			puts "Ok".green
		else
			raise "Jenkins package not found".red
		end
	end

	def prerequisities
		apt = Tools::Debian::Apt.new
		apt << "openjdk-7-jre" << "openjdk-7-jdk"
		apt.install
	end

	def install_cli
		puts "Looking for CLI".blue
		parser = Tools::Core::WebPageParser.new("http://127.0.0.1:8080/cli/")
		parser << ["jenkins-cli.jar"]
		package_url = parser.find_url
		if not package_url.empty?
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download("http://127.0.0.1:8080/#{package_url.first}")	
			path = "#{Dir.home}/.jenkins"
			puts "CLI path: #{path}"
			FileUtils::mkdir_p path
			FileUtils::cp package.path, path
		end
	end
end

end
end
end