require 'naturally'

module QDE
module Installer
module Applications

class Swarm
	def initialize(version = nil)
		@version = !version.nil? ? version : find_version
		@url = "http://maven.jenkins-ci.org/content/repositories/releases/org/jenkins-ci/plugins/swarm-client/#{@version}/"
	end

	def install
		puts "Looking for swarm cli: #{@url}".blue
		parser = Tools::Core::WebPageParser.new(@url)
		parser << ["swarm-client"]
		parser << ["jar-with-dependencies.jar"]
		package_url = parser.find_url
		if (not package_url.empty?)
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download(@url + package_url.first)
			FileUtils::mkdir_p '~/.jenkins'
			FileUtils::cp package.path, '~/.jenkins/'

			add_autostart('~/.jenkins/' + File.basename(package.path));

			puts "Ok".green
		else
			raise "Swarm CLI not found".red
		end
	end

	def find_version
		parser = Tools::Core::WebPageParser.new("http://maven.jenkins-ci.org/content/repositories/releases/org/jenkins-ci/plugins/swarm-client/")
		parser << ["1."] #["\\d\\.\\d+"]
		versions = parser.find_url
		if not versions.empty?
			Naturally.sort(versions).last
		else
			raise "Swarm CLI not found".red
		end	
	end

	def add_autostart(path)
		absolute_path = File.absolute_path(path)

		open("~/.jenkins/swarm-clien", 'w') do |f|
		  	f.puts '#!/bin/bash'
		  	f.puts "su"
		  	f.puts "java -jar #{absolute_path} &"
		end
		system("sudo mv ~/.jenkins/swarm-clien /etc/init.d/")
		system("sudo chmod +x /etc/init.d/swarm-clien")
		system("sudo update-rc.d /etc/init.d/swarn-client defaults")
		system("java -jar #{absolute_path} &")
	end
end

end
end
end