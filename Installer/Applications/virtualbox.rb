require 'colorize'
require 'open-uri'
require 'Tools/Core/web_page_parser.rb'
require 'Tools/Core/downloader.rb'
require 'Tools/Debian/info.rb'
require 'Tools/Debian/dpkg.rb'

module QDE
module Installer
module Applications 

class VirtualBox
	def initialize(version = nil)
		@version = version.nil? ? get_latest_version() : version
		@url = "http://download.virtualbox.org/virtualbox/#{@version}/"
	end

	def get_latest_version()
		open("http://download.virtualbox.org/virtualbox/LATEST.TXT") do | f |
			v = f.read
			v.strip!
		end
	end

	def install
		install_virtualbox()
		install_extpack()
	end

	def install_virtualbox
		puts "Looking for virtualbox: #{@url}".blue
		parser = Tools::Core::WebPageParser.new(@url)
		parser << Tools::Debian::Info.package_suffix
		parser << Tools::Debian::Info.architecture
		parser << ["virtualbox"]
		parser << ["Ubuntu"]
		parser << ["trusty", "saucy", "raring"]
		package_url = parser.find_url
		if (not package_url.empty?)
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download(@url + package_url.first)
			dpkg = Tools::Debian::Dpkg.new
			dpkg << package.path
			dpkg.install
			puts "Ok".green
		else
			raise "VirtualBox package not found".red
		end
	end

	def install_extpack()
		puts "Looking for virtualbox extension pack: #{@url}".blue
		parser = Tools::Core::WebPageParser.new(@url)
		parser << [".vbox-extpack"]
		package_url = parser.find_url
		if (not package_url.empty?)
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download(@url + package_url.first)
			system("sudo VBoxManage extpack install #{package.path}")
			puts "Ok".green
		else
			raise "VirtualBox extention package not found".red
		end
	end
end

end
end
end