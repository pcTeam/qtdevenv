require 'colorize'
require 'Tools/Core/web_page_parser.rb'
require 'Tools/Core/downloader.rb'
require 'Tools/Debian/info.rb'
require 'Tools/Debian/dpkg.rb'

module QDE
module Installer
module Applications 

class Vagrant
	def initialize(version = nil)
		@url = version.nil? ? "https://www.vagrantup.com/downloads.html" : "https://www.vagrantup.com/download-archive/v#{version}.html"
	end

	def install
		puts "Looking for vagrant: #{@url}".blue
		parser = Tools::Core::WebPageParser.new(@url)
		parser << Tools::Debian::Info.package_suffix
		parser << Tools::Debian::Info.architecture
		parser << ["vagrant"]
		package_url = parser.find_url
		if (not package_url.empty?)
			puts "Found: #{package_url.first}".green
			package = Tools::Core::Downloader.download(package_url.first)
			dpkg = Tools::Debian::Dpkg.new
			dpkg << package.path
			dpkg.install
			puts "Ok".green
		else
			raise "Vagrant package not found".red
		end
	end
end

end
end
end