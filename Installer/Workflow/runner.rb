require 'Installer/Master/runner.rb'
require 'Installer/BuildServer/runner.rb'

module QDE
module Installer
module Workflow

class Runner
	def initialize(options)
		@options = options
	end

	def run
		module_name = @options.module
		puts "Installing module: #{module_name}"
		if (module_name == nil || module_name == "master")
			runner = Master::Runner.new(@options)
			runner.run
		elsif module_name == "build-server"
			runner = BuildServer::Runner.new(@options)
			runner.run
		end
	end
end

end
end
end